import _ from 'lodash';
import "./style.css";
// import Temp from "./temp.jpg";
import Data from "./data.xml";

function component() {
  var element = document.createElement('div');

  // Lodash（目前通过一个 script 脚本引入）对于执行这一行是必需的
  element.innerHTML = _.join(['Hello', 'webpack'], ' ');
  element.classList.add('hello')

  // 将图像添加到现有的div
  // var myIcon = new Image();
  // myIcon.src = Temp;
  // element.append(myIcon);

  console.log(Data);
  return element;
}

document.body.appendChild(component());